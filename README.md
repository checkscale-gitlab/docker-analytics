# Docker IPLANFOR 

Imagem [Docker](https://www.docker.com) para ambiente de trabalho do IPLANFOR. Contém:

- [RStudio](https://www.rstudio.com)
- [PostgreSQL](https://www.postgresql.org)
- [pgAdmin](https://www.pgadmin.org)
- [ArangoDB](https://www.arangodb.com)

Para construir a imagem:
```
docker-compose up
```
